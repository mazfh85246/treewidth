//
// Created by matthew on 30.04.22.
//

#include <ctgmath>
#include <iostream>
#include "Graph.h"

void Graph::remove(int index) {
    unsigned int n = this->aList.size();
    // triangulate the neighbourhood of the index to be removed
    std::vector<int> neighbourhood;
    for (int i = 0; i < n; ++i) {
        if (this->aList[index][i]) {
            neighbourhood.push_back(i);
        }
    }

    for (int i = 0; i < neighbourhood.size(); ++i) {
        for (int j = i + 1; j < neighbourhood.size(); ++j) {
            if (!this->aList[neighbourhood[i]][neighbourhood[j]]) {
                this->aList[neighbourhood[i]][neighbourhood[j]] = true;
                this->aList[neighbourhood[j]][neighbourhood[i]] = true;
                this->degrees[neighbourhood[i]]++;
                this->degrees[neighbourhood[j]]++;
            }
        }
        // adjusts degree for future deletion
        this->degrees[neighbourhood[i]]--;
    }

    unsigned int last = this->aList.size() - 1;
    this->aList[index] = this->aList[last];
    this->aList.pop_back();

    for (std::vector<bool> &vertex: this->aList) {
        vertex[index] = vertex[last];
        vertex.pop_back();
    }
    this->degrees[index] = this->degrees[last];
    this->degrees.pop_back();
    if (!order.empty()){
        this->elimination_order.push_back(this->order[index]);
        this->order[index] = this->order[last];
        this->order.pop_back();
    }
}

std::ostream &operator<<(std::ostream &os, Graph &m) {
    if (m.order.empty()){
        os << '[';
        for (std::vector<bool> &row: m.aList) {
            os << '[';
            for (auto const &e: row) {
                os << (e ? '1' : '0') << ' ';
            }
            os << ']' << std::endl;
        }
        os << ']';
    }else{
        for (int i: m.elimination_order) {
            os << i << ',';
        }
    }
    return os;
}