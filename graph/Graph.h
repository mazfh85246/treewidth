//
// Created by matthew on 30.04.22.
//

#ifndef TREEWIDTH_GRAPH_H
#define TREEWIDTH_GRAPH_H


#include <vector>
#include <iostream>

class Graph {

public:
    std::vector<std::vector<bool> > aList;
    std::vector<int> degrees;
    std::vector<int> order;
    std::vector<int> elimination_order;
    Graph(std::vector<std::vector<bool> > &g, std::vector<int> &d) : aList(g), degrees(d) {};
    Graph(std::vector<std::vector<bool> > &g, std::vector<int> &d, std::vector<int> &order) : aList(g), degrees(d), order(order) {}
    void remove(int index);
};

std::ostream &operator<<(std::ostream &os, Graph &m);
#endif //TREEWIDTH_GRAPH_H
