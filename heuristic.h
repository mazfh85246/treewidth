//
// Created by matthew on 01.05.22.
//

#ifndef TREEWIDTH_HEURISTIC_H
#define TREEWIDTH_HEURISTIC_H

#include <vector>
#include "scoring/scoring_strategy.h"
#include <fstream>

namespace heuristic {
    void apply(scoring_strategy &s, Graph &g, std::vector<int> &curr, int max, bool print, std::ofstream &f,
               int lower_bound);
}
#endif //TREEWIDTH_HEURISTIC_H
