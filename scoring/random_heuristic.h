//
// Created by Akram Zaher Farid, Matthew on 22.08.22.
//

#ifndef TREEWIDTH_RANDOM_HEURISTIC_H
#define TREEWIDTH_RANDOM_HEURISTIC_H


#include "scoring_strategy.h"

class random_heuristic: public scoring_strategy {
    std::vector<int> getBest(Graph &g) override;
};


#endif //TREEWIDTH_RANDOM_HEURISTIC_H
