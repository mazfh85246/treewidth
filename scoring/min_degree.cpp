//
// Created by matthew on 30.04.22.
//

#include <valarray>
#include "min_degree.h"

std::vector<int> min_degree::getBest(Graph &g) {
    std::vector<int> best;
    // roughly the amount of possibilities that we are expecting. prevents a lot of relocation.
    best.reserve((size_t) sqrt(g.degrees.size()));
    int bestVal = g.degrees[0];
    for (int & degree : g.degrees) {
        bestVal = (bestVal > degree) ? degree : bestVal;
    }
    this->value = bestVal;
    for (int i = 0; i < g.degrees.size(); ++i) {
        if (g.degrees[i] == bestVal){
            best.push_back(i);
        }
    }
    //std::cout << "mfi:  " << bestVal << "\n";
    return best;
}
