#include "min_fill_in.h"

std::vector<int> MinFillIn::getBest(Graph &g) {
    int bestVal = g.degrees.size() * g.degrees.size();
    std::vector<int> best;
    std::vector<int> neighbours;
    for (size_t i = 0; i < g.degrees.size(); i++) {
        neighbours.clear();
        for (size_t j = 0; j < g.degrees.size(); j++) {
            if (g.aList[i][j]) {
                neighbours.push_back(j);
            }
        }

        int fill_in = 0;
        for (size_t j = 0; j < neighbours.size(); j++) {
            for (size_t k = j + 1; k < neighbours.size(); k++) {
                if (!g.aList[neighbours[j]][neighbours[k]]) {
                    fill_in++;
                }
            }
        }

        if (fill_in < bestVal) {
            bestVal = fill_in;
            best.clear();
            best.push_back(i);
        } else if (fill_in == bestVal) {
            best.push_back(i);
        }
    }

    return best;
}
