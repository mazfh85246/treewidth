//
// Created by matthew on 01.05.22.
//

#include "random_limited.h"

std::vector<int> random_limited::getBest(Graph &g) {
    auto best = ss->getBest(g);
    int acc_limit = 1 + (rand() % limit);
    while (best.size() > acc_limit) {
        unsigned int rnd = rand() % best.size();
        best[rnd] = best[best.size() - 1];
        best.pop_back();
    }
    return best;
}
