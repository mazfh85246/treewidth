//
// Created by matthew on 06.07.22.
//

#include "heuristic_with_ordering.h"

std::vector<int> heuristic_with_ordering::getBest(Graph &g) {
    auto results = ss->getBest(g);

    for (int i: ordering) {
        for (int j: results) {
            if (i == g.order[j]) {
                return {j};
            }
        }
    }

    return results;
}
