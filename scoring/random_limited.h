//
// Created by matthew on 01.05.22.
//

#ifndef TREEWIDTH_RANDOM_LIMITED_H
#define TREEWIDTH_RANDOM_LIMITED_H


#include "scoring_strategy.h"

class random_limited : public scoring_strategy {
private:
    int value = 0;
    int limit = 5;
    scoring_strategy *ss;
public:
    random_limited(int _limit, scoring_strategy &ss) : limit(_limit), ss(&ss) {}

    std::vector<int> getBest(Graph &g) override;
};


#endif //TREEWIDTH_RANDOM_LIMITED_H
