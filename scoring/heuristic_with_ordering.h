//
// Created by matthew on 06.07.22.
//

#ifndef TREEWIDTH_HEURISTIC_WITH_ORDERING_H
#define TREEWIDTH_HEURISTIC_WITH_ORDERING_H


#include <utility>

#include "scoring_strategy.h"

class heuristic_with_ordering : public scoring_strategy {
private:
    std::vector<int> ordering;
    scoring_strategy *ss;

public:
    heuristic_with_ordering(std::vector<int> ordering, scoring_strategy *ss) : ordering(std::move(ordering)),
                                                                               ss(ss) {}

    std::vector<int> getBest(Graph &g) override;
};


#endif //TREEWIDTH_HEURISTIC_WITH_ORDERING_H
