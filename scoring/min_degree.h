//
// Created by matthew on 30.04.22.
//

#ifndef TREEWIDTH_MIN_DEGREE_H
#define TREEWIDTH_MIN_DEGREE_H


#include "scoring_strategy.h"

class min_degree : public scoring_strategy {
private:
    int value = 0;
public:
    std::vector<int> getBest(Graph &g) override;
};


#endif //TREEWIDTH_MIN_DEGREE_H
