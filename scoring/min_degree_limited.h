//
// Created by matthew on 01.05.22.
//

#ifndef TREEWIDTH_MIN_DEGREE_LIMITED_H
#define TREEWIDTH_MIN_DEGREE_LIMITED_H


#include "scoring_strategy.h"

class min_degree_limited : public scoring_strategy{
private:
    int value = 0;
    int limit = 5;
public:
    min_degree_limited(int _limit) : limit(_limit){}
    std::vector<int> getBest(Graph &g) override;
};


#endif //TREEWIDTH_MIN_DEGREE_LIMITED_H
