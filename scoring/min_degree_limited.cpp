//
// Created by matthew on 01.05.22.
//

#include <valarray>
#include "min_degree_limited.h"

std::vector<int> min_degree_limited::getBest(Graph &g) {
    // TODO: refactor to use any heuristic
    std::vector<int> best;
    int bestVal = g.degrees[0];
    for (int &degree: g.degrees) {
        bestVal = (bestVal > degree) ? degree : bestVal;
    }
    this->value = bestVal;
    for (int i = 0; i < g.degrees.size(); ++i) {
        if (g.degrees[i] == bestVal) {
            best.push_back(i);
        }
    }
    if (best.size() <= limit) {
        return best;
    }
    for (int i = 0; i < limit; ++i) {
        unsigned int rnd = (rand() % (best.size() - i)) + i;
        auto temp = best[i];
        best[i] = best[rnd];
        best[rnd] = temp;
    }

    best.erase(best.begin() + limit - 1, best.end());
    return best;
}