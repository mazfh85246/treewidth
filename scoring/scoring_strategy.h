//
// Created by matthew on 30.04.22.
//

#ifndef TREEWIDTH_SCORING_STRATEGY_H
#define TREEWIDTH_SCORING_STRATEGY_H

#include <vector>
#include "../graph/Graph.h"

/**
 * Scores a vertex on a certain criteria
 */
class scoring_strategy {
public:
    virtual std::vector<int> getBest(Graph &g) = 0;
};


#endif //TREEWIDTH_SCORING_STRATEGY_H
