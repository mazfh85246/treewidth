#include "scoring_strategy.h"

#ifndef MIN_FILL_IN
#define MIN_FILL_IN

class MinFillIn: public scoring_strategy{
private:
    int value = 0;
public:
    MinFillIn(/* args */) = default;
    ~MinFillIn() = default;
    std::vector<int> getBest(Graph &g) override;
};


#endif