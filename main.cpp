#include <iostream>
#include "graph/Graph.h"
#include "heuristic.h"
#include "scoring/min_degree.h"
#include "scoring/random_limited.h"
#include "scoring/min_fill_in.h"
#include "scoring/heuristic_with_ordering.h"
#include "scoring/random_heuristic.h"

Graph generateGrid(int m, int n) {
    const int size = n * m;
    std::vector<std::vector<bool> > rows(size);
    for (int i = 0; i < size; ++i) {
        rows[i] = std::vector<bool>(size, false);
        auto col = &rows[i];
        if ((i % m) + 1 < m) {
            (*col)[i + 1] = true;
        }
        if ((i % m) - 1 >= 0) {
            (*col)[i - 1] = true;
        }
        if (i + m < size) {
            (*col)[i + m] = true;
        }
        if (i - m >= 0) {
            (*col)[i - m] = true;
        }
    }
    std::vector<int> degrees(size, 0);

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            if (rows[i][j]) degrees[i]++;
        }
    }

    std::vector<int> order;
    order.reserve(n * m);
    for (int i = 0; i < n * m; ++i) {
        order.push_back(i);
    }

    return {rows, degrees, order};
}

std::vector<int> generateOrdering(int n, int m) {
    std::vector<int> out;

    int nn = (n / 2) - 1;

    for (int i = 0; i < nn; ++i) {
        int x = i;
        int y = 0;
        while (x >= 0 && y < m) {
            out.push_back(x * m + y);
            x--;
            y++;
        }

        x = i;
        y = m - 1;
        while (x >= 0 && y >= 0) {
            out.push_back(x * m + y);
            x--;
            y--;
        }
        // top is sorted. Now to sort out the bottom

        x = n - 1;
        y = i;

        while (x >= 0 && y >= 0) {
            out.push_back(x * m + y);
            x--;
            y--;
        }

        x = n - 1;
        y = m - i - 1;

        while (x >= 0 && y < m) {
            out.push_back(x * m + y);
            x--;
            y++;
        }
    }
    return out;
}

Graph generateReinforcedGrid(int m, int n) {
    const int size = n*m;
    std::vector<std::vector<bool>> rows(size);
    rows[0] = std::vector<bool>(size, true);
    rows[0][0] = false;
    for (int i = 1; i < size; ++i) {
        rows[i] = std::vector<bool>(size, false);
        auto col = &rows[i];
        if ((i % m) + 1 < m) {
            (*col)[i + 1] = true;
        }
        if ((i % m) - 1 >= 0) {
            (*col)[i - 1] = true;
        }
        if (i + m < size) {
            (*col)[i + m] = true;
        }
        if (i - m >= 0) {
            (*col)[i - m] = true;
        }

        if ((i % m) + 1 < m && i + m < size){
            (*col)[i+m+1] = true;
        }

        if ((i % m) - 1 >= 0 && i - m >= 0){
            (*col)[i-m-1] = true;
        }
    }

    std::vector<int> degrees(size, 0);

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            if (rows[i][j]) degrees[i]++;
        }
    }

    std::vector<int> order;
    order.reserve(n * m);
    for (int i = 0; i < n * m; ++i) {
        order.push_back(i);
    }

    return {rows, degrees, order};
}


int main(int argc, char *argv[]) {
    int indices[] = {5,10,15,20,25,30,40,50,60,70,90,120,150,200};
    for (int k: indices) {
        srand(time(nullptr));

        std::ofstream f("../../../Desktop/data/md" + std::to_string(k));
        //std::ofstream f("da.txt");
        for (int j = 0; j < 1; j++) {
            Graph g(generateGrid(k, k));
            MinFillIn mfi;
            min_degree md;
            random_heuristic r;
            //heuristic_with_ordering hwo(generateOrdering(k, k), &r);
            random_limited limited(1, md);
            std::vector<int> result;
            heuristic::apply(limited, g, result, 0, false, f, std::min(k, k));
        }
        std::cout << "done up to : " << k << std::endl;
    }
    return 0;
}
