//
// Created by matthew on 01.05.22.
//


#include "heuristic.h"

void heuristic::apply(scoring_strategy &s, Graph &g, std::vector<int> &curr, int max, bool print, std::ofstream &f,
                      int lower_bound) {
    std::vector<int> ind(s.getBest(g));

    if (g.degrees.size() <= lower_bound) {
        if (print) {
            f << g;
            f << "      " << max;
            f << '\n';
        }else{
            f << max << "\n";
        }
        curr.push_back(max);
        return;
    }
    for (int i = 0; i < ind.size(); i++) {
        int index = ind[i];
        Graph *ng;
        if (i == ind.size() - 1) {
            ng = &g;
        } else {
            ng = new Graph(g);
        }
        ng->remove(index);

        int degree = g.degrees[index];
        max = (degree > max) ? degree : max;
        apply(s, *ng, curr, max, print, f, std::max(lower_bound, max));

        if (i != ind.size() - 1) {
            delete ng;
        }
    }
}
